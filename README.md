# waimanu.cloud

## Description
Manuel and first documentation stuff for the waimanu.cloud project.

## Structure

```
.
├── .ssh        # SSH config dir
│   ├── id      # Symlink for private ssh key¹
│   └── id.pub  # Symlink for public ssh key¹
└── README.md   # Readme
```

## Member

* **sliven**: `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO5QGgpFMBajQPGv5/6zry89Tpbnclui76tLiEPRexiG`
* **yosijo**: `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP8hN+r+7YdeHL7SmdTMDHnnHjw68DSsD8z/DD1hSXZj`

## Service & Provider of project

* [Codeberg](https://codeberg.org/)
  * [Repository](https://codeberg.org/waimanu.cloud/main)
* [Hetzner](https://www.hetzner.de/)
  * Rootserver
* [INWX](https://www.inwx.de/)
  * [Domain](https://waimanu.cloud/)

## Step by step

1. Go to Codeberg and create a repo for your project.
2. Clone and join the repository.
```
git clone git@codeberg.org:waimanu.cloud/main.git waimanu.cloud
cd waimanu.cloud
```
3. Create a ssh key under the `.ssh` directory with the `id_` Prefix.
```
ssh-keygen -t ed25519 -f ".ssh/id_${ssh_key_name?}"
```
4. Create a Symlink for the ssh key.
```
cd .ssh
ln -s "id_${ssh_key_name?}" id
ln -s "id_${ssh_key_name?}.pub" id.pub
```
5. Fill the README.md
6. Order a new server by hetzner and use the ssh public key.

## Footnotes

1. Member of gitignore list
